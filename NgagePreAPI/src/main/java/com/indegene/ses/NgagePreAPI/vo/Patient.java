package com.indegene.ses.NgagePreAPI.vo;

/**
 * 
 * @author vinaya.math
 *
 */
public class Patient {
	private String id;
	private String first_name;
	private String last_name;
	private String dob;
	private String mobile;
	private String address;
	private String address2;
	private String city;
	private String country;
	private String channelPreference;
	private int concent;
	private String careGiver;
	private String relationshipPatient;
	private String prescribed;
	private String campaignId;
	private String landline_Number;
	private String careGiverDOB;
	private String careGiverMobile;
	private String eirCode;

	public Patient() {

	}

	public Patient(String id, String first_name, String last_name, String dob, String mobile, String address,
			String address2, String city, String country, String channelPreference, int concent, String careGiver,
			String relationshipPatient, String prescribed, String campaignId,String landline_Number,String careGiverDOB,String careGiverMobile,
			String eirCode) {
		this.id = id;
		this.first_name = first_name;
		this.last_name = last_name;
		this.dob = dob;
		this.mobile = mobile;
		this.address = address;
		this.address2 = address2;
		this.city = city;
		this.country = country;
		this.channelPreference = channelPreference;
		this.concent = concent;
		this.careGiver = careGiver;
		this.relationshipPatient = relationshipPatient;
		this.prescribed = prescribed;
		this.campaignId = campaignId;
		this.landline_Number = landline_Number;
		this.careGiverDOB = careGiverDOB;
		this.careGiverMobile = careGiverMobile;
		this.eirCode=eirCode;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getChannelPreference() {
		return channelPreference;
	}

	public void setChannelPreference(String channelPreference) {
		this.channelPreference = channelPreference;
	}

	public int getConcent() {
		return concent;
	}

	public void setConcent(int concent) {
		this.concent = concent;
	}

	public String getCareGiver() {
		return careGiver;
	}

	public void setCareGiver(String careGiver) {
		this.careGiver = careGiver;
	}

	public String getRelationshipPatient() {
		return relationshipPatient;
	}

	public void setRelationshipPatient(String relationshipPatient) {
		this.relationshipPatient = relationshipPatient;
	}

	public String getPrescribed() {
		return prescribed;
	}

	public void setPrescribed(String prescribed) {
		this.prescribed = prescribed;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public String getLandline_Number() {
		return landline_Number;
	}

	public void setLandline_Number(String landline_Number) {
		this.landline_Number = landline_Number;
	}

	public String getCareGiverDOB() {
		return careGiverDOB;
	}

	public void setCareGiverDOB(String careGiverDOB) {
		this.careGiverDOB = careGiverDOB;
	}

	public String getCareGiverMobile() {
		return careGiverMobile;
	}

	public void setCareGiverMobile(String careGiverMobile) {
		this.careGiverMobile = careGiverMobile;
	}

	public String getEirCode() {
		return eirCode;
	}

	public void setEirCode(String eirCode) {
		this.eirCode = eirCode;
	}

}
