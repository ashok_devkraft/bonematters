package com.indegene.ses.NgagePreAPI.common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import java.util.TimeZone;

public class ConnectionMannager {

	public static Connection GetConnection() {

		Connection connection = null;
		try {
			// FileReader reader = new FileReader("/application.properties");
			Properties properties = new Properties();
			// properties.load(reader);
			properties.load(ConnectionMannager.class.getClassLoader().getResourceAsStream("application.properties"));
			String dbTimeZone = properties.getProperty("ngage.jdbc.time_zone");
			String dbHost = properties.getProperty("ngage.jdbc.host");
			String dbUser = properties.getProperty("ngage.jdbc.username");
			String dbPass = properties.getProperty("ngage.jdbc.password");

			TimeZone timeZone = TimeZone.getTimeZone(dbTimeZone);
			TimeZone.setDefault(timeZone);
			Class.forName("oracle.jdbc.driver.OracleDriver");

			// ngage-PROD
//			connection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:7768:ORCL", "NGAGE_PD",
//			"ind3g3n3123");

//			connection = DriverManager.getConnection(
//					"jdbc:oracle:thin:@ngage-prd.ccywsqg1yrkj.eu-west-2.rds.amazonaws.com:1521:ORCL", "NGAGE_PD",
//					"ind3g3n3123");

			// ngage-test
			connection = DriverManager.getConnection(dbHost, dbUser, dbPass);

			// Dev1
//			connection = DriverManager.getConnection("jdbc:oracle:thin:@emceqa.clzut2savnin.us-east-1.rds.amazonaws.com:1521:ORCL", "PSP_DEV1",
//					"ind3g3n3123");

			// Dev6
//			connection = DriverManager.getConnection("jdbc:oracle:thin:@192.0.0.86:1521:xe", "NGAGE_DEV6","ind3g3n3123");

			// local
//			connection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:7786:ORCVSANO", "NGAGE_PD",
//					"ind3g3n3123");

			// ngage-Devcraft
//			connection = DriverManager.getConnection(
//					"jdbc:oracle:thin:@database-1.c8ryoryjzjw8.ap-south-1.rds.amazonaws.com:1521:MYDB", "admin",
//					"2)9c(qG)");
		} catch (Exception e) {
			System.err.println(e);
			e.printStackTrace();
		}
		return connection;
	}

	public static String getNextId(String query) {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String value = null;
		int id = 0;
		connection = GetConnection();
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				value = resultSet.getString(1);
				if (value != null) {
					id = Integer.parseInt(value);
					value = Integer.toString(++id);
				} else {
					value = Integer.toString(++id);
				}
			}
			connection.close();
		} catch (Exception e) {
			System.err.println(e);
			e.printStackTrace();
		}

		return value;
	}
}
