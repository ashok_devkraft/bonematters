package com.indegene.ses.NgagePreAPI.service.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.indegene.ses.NgagePreAPI.common.CommonResponse;
import com.indegene.ses.NgagePreAPI.common.ConnectionMannager;
import com.indegene.ses.NgagePreAPI.enums.StatusEnum;
import com.indegene.ses.NgagePreAPI.service.AuthService;
import com.indegene.ses.NgagePreAPI.service.BatchCodeService;
import com.indegene.ses.NgagePreAPI.utils.AESUtils;
import com.indegene.ses.NgagePreAPI.vo.BatchCodeParam;

@Service
public class BatchCodeServiceImpl implements BatchCodeService {

	private static final Logger LOGGER = Logger.getLogger(BatchCodeServiceImpl.class);

	@Autowired
	public AuthService authService;

	@Override
	public CommonResponse getBatchCodeDetailsByCode(BatchCodeParam batchCode, String token) {
		LOGGER.debug("getting batch code details");
		CommonResponse response = new CommonResponse();
		try {
			authService.parseToken(token);
			String code = AESUtils.decryption(batchCode.getCode());
			String campaignName = null;
			BatchCodeParam batchCodeParam = getBatchCodeByCode(code);
			if (batchCodeParam != null) {
				if (isBatchCodeUsed(batchCodeParam)) {
					campaignName = getCamapinNameByBatchCodeId(batchCodeParam.getId());
					batchCodeParam.setCamapignName(campaignName);
					response.setStatus(StatusEnum.SUCCESS);
					response.setResponse(batchCodeParam);
				} else {
					response.setStatus(StatusEnum.FAILED);
					response.setErrorCode("ERROR");
					response.setErrorMessage("This is a new batch code please click Register");
				}
			} else {
				response.setStatus(StatusEnum.FAILED);
				response.setErrorCode("NOT_FOUND");
				response.setErrorMessage("This is an invalid batch code");
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			e.getStackTrace();
			response.setStatus(StatusEnum.FAILED);
			response.setErrorCode("ERROR");
			response.setErrorMessage("Error: This is an invalid batch code");
		}
		return response;
	}

	private String getCamapinNameByBatchCodeId(String id) {
		LOGGER.debug("Getting campaing name by batchcode id:" + id);
		String response = null;
		Connection connection = ConnectionMannager.GetConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		String query = "select CAMPAIGN_NAME from CAMPAIGN WHERE CAMPAIGN_ID = (select CAMPAIGN_ID from BATCH_CODE_PATIENT where BATCH_CODE_ID = "
				+ id + ")";
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				response = resultSet.getString(1);
			}
			connection.close();
		} catch (SQLException e) {
			LOGGER.error(e);
			e.printStackTrace();
		}
		return response;
	}

	private boolean isBatchCodeUsed(BatchCodeParam batchCodeParam) {
		LOGGER.debug("Checking is code used");
		boolean status = false;
		Connection connection = ConnectionMannager.GetConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		String query = "SELECT ID FROM BATCH_CODE_PATIENT WHERE BATCH_CODE_ID IN (SELECT ID FROM BATCH_CODE WHERE STATUS = 1 AND CODE = '"
				+ batchCodeParam.getCode() + "')";
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				status = true;
			}
			connection.close();
		} catch (SQLException e) {
			LOGGER.error(e);
			e.printStackTrace();
		}
		return status;
	}

	private BatchCodeParam getBatchCodeByCode(String code) throws SQLException {
		LOGGER.debug("Getting batch code details from DB with Code:" + code);

		BatchCodeParam batchCode = null;
		if (code == null) {
			return null;
		}
		code = code.trim();
		if (code.length() > 10) {
			return null;
		}

		try {
			Integer.parseInt(code);
		} catch (NumberFormatException nfe) {
			return null;
		}

		Connection connection = ConnectionMannager.GetConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		String query = "SELECT ID, CODE, STATUS FROM BATCH_CODE WHERE CODE = " + code;

		statement = connection.createStatement();
		resultSet = statement.executeQuery(query);
		while (resultSet.next()) {
			batchCode = new BatchCodeParam();
			batchCode.setId(resultSet.getString(1));
			batchCode.setCode(resultSet.getString(2));
			batchCode.setStatus(resultSet.getBoolean(3));
		}
		connection.close();

		return batchCode;
	}

}
