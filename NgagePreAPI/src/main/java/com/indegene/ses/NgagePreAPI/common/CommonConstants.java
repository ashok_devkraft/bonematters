package com.indegene.ses.NgagePreAPI.common;

public class CommonConstants {
	public static final String NGAGE_PSO_PHYSICIAN_ID		= "ngage.pso.physician.id";
	public static final String NGAGE_PSO_CAMPAIN_ID 		= "ngage.pso.campain.id";
	public static final String NGAGE_PSA_PHYSICIAN_ID		= "ngage.psa.physician.id";
	public static final String NGAGE_PSA_CAMPAIN_ID 		= "ngage.psa.campain.id";
	public static final String NGAGE_ACCESS_TOKEN_URL 		= "ngage.access.token.url";
	public static final String NGAGE_ACCESS_CLIENT_NAME 	= "ngage.access.client.name";
	public static final String NGAGE_ACCESS_USERNAME 		= "ngage.access.username";
	public static final String NGAGE_ACCESS_PASSWORD 		= "ngage.access.password";
	public static final String NGAGE_PATIENT_ENROLLMENT_URL = "ngage.patient.enrollment.url";
	public static final String NGAGE_MSITE_CLIENT_ID	 	= "ngage.msite.client.id";
	public static final String NGAGE_MSITE_CLIENT_SECRET 	= "ngage.msite.client.secret";
	public static final String NGAGE_SMS_INVALID_INPUT 		= "ngage.sms.invalid.input";
	public static final String NGAGE_PROLIO_PHYSICIAN_ID	= "ngage.prolio.physician.id";
	public static final String NGAGE_PROLIO_CAMPAIN_ID 		= "ngage.prolio.campain.id";

}
