package com.indegene.ses.NgagePreAPI.vo;

import java.io.Serializable;

public class CampaignEnrollVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long campaignID;
	private int successCount;
	private int activeCount;

	public CampaignEnrollVO() {

	}

	public CampaignEnrollVO(Long campaignID, int successCount, int activeCount) {
		this.campaignID = campaignID;
		this.successCount = successCount;
		this.activeCount = activeCount;
	}

	public Long getCampaignID() {
		return campaignID;
	}

	public void setCampaignID(Long campaignID) {
		this.campaignID = campaignID;
	}

	public int getSuccessCount() {
		return successCount;
	}

	public void setSuccessCount(int successCount) {
		this.successCount = successCount;
	}

	public int getActiveCount() {
		return activeCount;
	}

	public void setActiveCount(int activeCount) {
		this.activeCount = activeCount;
	}

}
