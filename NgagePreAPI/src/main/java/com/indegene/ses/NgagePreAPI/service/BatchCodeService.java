package com.indegene.ses.NgagePreAPI.service;

import com.indegene.ses.NgagePreAPI.common.CommonResponse;
import com.indegene.ses.NgagePreAPI.vo.BatchCodeParam;

public interface BatchCodeService {
	public CommonResponse getBatchCodeDetailsByCode(BatchCodeParam batchCode, String token);
}
