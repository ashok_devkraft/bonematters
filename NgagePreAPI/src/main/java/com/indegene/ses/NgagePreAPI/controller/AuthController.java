package com.indegene.ses.NgagePreAPI.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.indegene.ses.NgagePreAPI.service.AuthService;
import com.indegene.ses.NgagePreAPI.vo.OAuthTokenVo;
import com.indegene.ses.NgagePreAPI.vo.TokenParam;

@RestController
@RequestMapping("/auth")
public class AuthController {
	private static final Logger LOGGER = Logger.getLogger(AuthController.class);

	@Autowired
	public AuthService authService;

	@PostMapping("/token")
	public OAuthTokenVo token(@RequestBody TokenParam param) {
		LOGGER.info("Started authenticating");
		OAuthTokenVo oAuthTokenVo = authService.getToken(param);
		return oAuthTokenVo;
	}
}
