package com.indegene.ses.NgagePreAPI.service;

import com.indegene.ses.NgagePreAPI.common.CommonResponse;
import com.indegene.ses.NgagePreAPI.vo.Patient;
import com.indegene.ses.NgagePreAPI.vo.SearchVO;

public interface PatientService {

	public CommonResponse register(Patient patient, String token);
	
	public CommonResponse mobileVerify(Patient patient, String token);
	
	public CommonResponse search(SearchVO search, String token);
	
	public CommonResponse unsubscribe(Patient patient, String token);
}
