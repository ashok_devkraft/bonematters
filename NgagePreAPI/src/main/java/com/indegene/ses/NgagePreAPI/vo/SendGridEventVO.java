package com.indegene.ses.NgagePreAPI.vo;

public class SendGridEventVO {

	private String ngagEmailUniqueId;
	private String event;
	private String email;

	public SendGridEventVO() {

	}

	public SendGridEventVO(String ngagEmailUniqueId, String event, String email) {
		this.ngagEmailUniqueId = ngagEmailUniqueId;
		this.event = event;
		this.email = email;
	}

	public String getNgagEmailUniqueId() {
		return ngagEmailUniqueId;
	}

	public void setNgagEmailUniqueId(String ngagEmailUniqueId) {
		this.ngagEmailUniqueId = ngagEmailUniqueId;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
