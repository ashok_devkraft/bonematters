package com.indegene.ses.NgagePreAPI.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.indegene.ses.NgagePreAPI.common.CommonResponse;
import com.indegene.ses.NgagePreAPI.service.PatientService;
import com.indegene.ses.NgagePreAPI.vo.Patient;
import com.indegene.ses.NgagePreAPI.vo.SearchVO;

@RestController
@RequestMapping("/patient")
public class PatientController {
	private static final Logger LOGGER = Logger.getLogger(PatientController.class);

	@Autowired
	public PatientService patientService;

	@PostMapping("/details")
	public CommonResponse register(@RequestBody Patient patient, @RequestHeader("Authorization") String token) {
		LOGGER.info("Started registering the patient details");
		CommonResponse response = patientService.register(patient, token);
		LOGGER.info("Completed registering the patient details with Status:" + response.getStatus());
		return response;
	}

	@PostMapping("/verify")
	public CommonResponse phoneVerify(@RequestBody Patient patient, @RequestHeader("Authorization") String token) {
		LOGGER.info("Started verifying phone number");
		CommonResponse response = patientService.mobileVerify(patient, token);
		LOGGER.info("Completed verifying phone number with Status:" + response.getStatus());
		return response;
	}

	@PostMapping("/search")
	public CommonResponse search(@RequestBody SearchVO search, @RequestHeader("Authorization") String token) {
		LOGGER.info("Started searching patient details");
		CommonResponse response = patientService.search(search, token);
		LOGGER.info("Completed patient details search with Status:" + response.getStatus());
		return response;
	}

	@PostMapping("/unsubscribe")
	public CommonResponse unsubscribe(@RequestBody Patient patient, @RequestHeader("Authorization") String token) {
		LOGGER.info("Started unsubscribing the patient from campaign");
		CommonResponse response = patientService.unsubscribe(patient, token);
		LOGGER.info("Completed unsubscribing the patient from campaign with Status:" + response.getStatus());
		return response;
	}
}
