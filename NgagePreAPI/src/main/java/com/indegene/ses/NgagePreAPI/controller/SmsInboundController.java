package com.indegene.ses.NgagePreAPI.controller;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.indegene.ses.NgagePreAPI.service.SmsInboundService;

@RestController
@RequestMapping("/sms")
public class SmsInboundController {
	private static final Logger LOGGER = Logger.getLogger(SmsInboundController.class);

	@Autowired
	public SmsInboundService smsInboundService;

	@PostMapping(value = "/reply", consumes = { MediaType.APPLICATION_FORM_URLENCODED_VALUE }, produces = {
			MediaType.TEXT_PLAIN_VALUE })
	public String handler(@RequestBody String body) {
		LOGGER.info("Started processing the request");
		String reply = smsInboundService.processRequest(body);
		LOGGER.info("Completed request");
		return reply;
	}

	@PostMapping(value = "/status", consumes = { MediaType.APPLICATION_FORM_URLENCODED_VALUE }, produces = {
			MediaType.TEXT_PLAIN_VALUE })
	public String statusHandler(@RequestBody String body, HttpServletResponse response) {
		LOGGER.info("Started processing the request");
		String status = "";
		smsInboundService.processStatusRequest(body);
		LOGGER.info("Completed request");
		return status;
	}
}
