package com.indegene.ses.NgagePreAPI.service.impl;

import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.indegene.ses.NgagePreAPI.common.CommonConstants;
import com.indegene.ses.NgagePreAPI.service.AuthService;
import com.indegene.ses.NgagePreAPI.vo.OAuthTokenVo;
import com.indegene.ses.NgagePreAPI.vo.TokenParam;

@Service
public class AuthServiceImpl implements AuthService {
	private static final Logger LOGGER = Logger.getLogger(AuthServiceImpl.class);

	@Value("${" + CommonConstants.NGAGE_MSITE_CLIENT_ID + "}")
	private String clientId;

	@Value("${" + CommonConstants.NGAGE_MSITE_CLIENT_SECRET + "}")
	private String clientSecret;

	private String algorithmKey = "R)t\\\\y/4aRZfN6dZY";

	private String issuerKey = "https://www.indegene.com";

	@Override
	public OAuthTokenVo getToken(TokenParam tokenParam) {
		LOGGER.debug("Generating the token");
		OAuthTokenVo oAuthTokenVo = new OAuthTokenVo();
		String token = null;
		try {
			String isParamValid = validateParam(tokenParam);
			if (isParamValid.equalsIgnoreCase("VALID")) {
				Date date = new Date();
				Calendar c = Calendar.getInstance();
				c.setTime(date);
				c.add(Calendar.HOUR, 1);
				Date expireDate = c.getTime();
				Algorithm algorithm = Algorithm.HMAC256(algorithmKey);

				token = JWT.create().withKeyId("CnDhSshmFnK4nquAn0SBkcGyHdZRkFL3f0x63vFF").withSubject(clientId)
						.withIssuer(issuerKey).withIssuedAt(date).withExpiresAt(expireDate).sign(algorithm);
				oAuthTokenVo.setAccess_token(token);
				LOGGER.debug("Token generated :" + token);
			} else {
				throw new Exception("Invalid Param");
			}
		} catch (JWTCreationException e) {
			LOGGER.error(e);
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e);
			e.printStackTrace();
		}

		return oAuthTokenVo;
	}

	@Override
	public void parseToken(String token) {
		LOGGER.debug("Parsing the token:" + token);
		token = token.contains("Bearer ") == true ? token.substring(7) : token;
		token = token.contains("bearer ") == true ? token.substring(7) : token;
		Algorithm algorithm = Algorithm.HMAC256(algorithmKey);
		JWTVerifier verifier = JWT.require(algorithm).withIssuer(issuerKey).build();
		DecodedJWT jwt = verifier.verify(token);
	}

	private String validateParam(TokenParam tokenParam) {
		String response = "";
		if (tokenParam == null) {
			response = "PARAM_REQUIRED";
		} else if (tokenParam.getClient_id() == null) {
			response = "CLIENT_ID_REQUIRED";
		} else if (tokenParam.getClient_id().trim().isEmpty()) {
			response = "CLIENT_ID_EMPTY";
		} else if (!tokenParam.getClient_id().trim().equals(clientId)) {
			response = "INVALID_CLIENT_ID";
		} else if (tokenParam.getClient_secret() == null) {
			response = "CLIENT_SECRET_REQUIRED";
		} else if (tokenParam.getClient_secret().trim().isEmpty()) {
			response = "CLIENT_SECRET_EMPTY";
		} else if (!tokenParam.getClient_secret().trim().equals(clientSecret)) {
			response = "INVALID_CLIENT_SECRET";
		} else {
			response = "VALID";
		}
		return response;
	}
}
