package com.indegene.ses.NgagePreAPI.vo;

public class SmsVO {
	private String messageSid;
	private String accountSid;
	private String messagingServiceSid;
	private String from;
	private String to;
	private String body;
	private String status;

	public SmsVO() {

	}

	public SmsVO(String messageSid, String accountSid, String messagingServiceSid, String from, String to, String body,
			String status) {
		this.messageSid = messageSid;
		this.accountSid = accountSid;
		this.messagingServiceSid = messagingServiceSid;
		this.from = from;
		this.to = to;
		this.body = body;
		this.setStatus(status);
	}

	public String getMessageSid() {
		return messageSid;
	}

	public void setMessageSid(String messageSid) {
		this.messageSid = messageSid;
	}

	public String getAccountSid() {
		return accountSid;
	}

	public void setAccountSid(String accountSid) {
		this.accountSid = accountSid;
	}

	public String getMessagingServiceSid() {
		return messagingServiceSid;
	}

	public void setMessagingServiceSid(String messagingServiceSid) {
		this.messagingServiceSid = messagingServiceSid;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
