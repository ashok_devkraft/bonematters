package com.indegene.ses.NgagePreAPI.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.indegene.ses.NgagePreAPI.common.CommonResponse;
import com.indegene.ses.NgagePreAPI.service.EmailInboundService;

@RestController
@RequestMapping("/email")
public class EmailInboundController {
	private static final Logger LOGGER = Logger.getLogger(EmailInboundController.class);

	@Autowired
	public EmailInboundService emailInboundService;

	@PostMapping("/event")
	public void handler(@RequestBody String body) {
		LOGGER.info("Started processing the request");
		emailInboundService.processRequest(body);
		LOGGER.info("Completed request");
	}

	@GetMapping(value = "/unsubscribe/{link}")
	public CommonResponse unsubscribe(@PathVariable("link") String link) {
		LOGGER.info("Started unsubscribing for link:" + link);
		CommonResponse response = emailInboundService.unsubscribe(link);
		LOGGER.info("Completed unsubscribing with Status:" + response.getStatus());
		return response;
	}
}
