package com.indegene.ses.NgagePreAPI.service.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.auth0.jwt.exceptions.TokenExpiredException;
import com.indegene.ses.NgagePreAPI.common.CommonConstants;
import com.indegene.ses.NgagePreAPI.common.CommonResponse;
import com.indegene.ses.NgagePreAPI.common.ConnectionMannager;
import com.indegene.ses.NgagePreAPI.enums.StatusEnum;
import com.indegene.ses.NgagePreAPI.service.AuthService;
import com.indegene.ses.NgagePreAPI.service.PatientService;
import com.indegene.ses.NgagePreAPI.vo.Patient;
import com.indegene.ses.NgagePreAPI.vo.SearchVO;

@Service
public class PatientServiceImpl implements PatientService {

	private static final Logger LOGGER = Logger.getLogger(PatientServiceImpl.class);

	@Autowired
	public AuthService authService;

	@Value("${" + CommonConstants.NGAGE_PROLIO_PHYSICIAN_ID + "}")
	private String prolioPhysicianId;

	@Value("${" + CommonConstants.NGAGE_PROLIO_CAMPAIN_ID + "}")
	private String prolioCampainId;

	@Override
	public CommonResponse register(Patient patient, String token) {
		LOGGER.debug("Registration Started");
		CommonResponse response = new CommonResponse();
		Connection connection = null;
		try {
			authService.parseToken(token);
			
			String ispatientValidated = patientValidation(patient);
			if (ispatientValidated.equalsIgnoreCase("VALID")) {
				connection = ConnectionMannager.GetConnection();
				connection.setAutoCommit(false);
				int isSuccessPatient = insertPatient(connection, patient);
				int isSuccessPatientAddress = insertPatientAddress(connection, patient);
				int isSuccessPatientMobile = insertPatientMobile(connection, patient);
				int isSuccessPatientPhysician = insertPatientPhysician(connection, patient);
				int isSuccessinsertPatientChannel = insertPatientChannel(connection, patient);
				if (isSuccessPatient == 1 && isSuccessPatientAddress == 1 && isSuccessPatientMobile == 1
						&& isSuccessPatientPhysician == 1 && isSuccessinsertPatientChannel >= 1) {
					connection.commit();
					LOGGER.debug("Patient details successful saved");
					// int isAutoEnrolled = ngageService.campainEnroll(patient);
					int isAutoEnrolled = 1;
					if (isAutoEnrolled == 1) {
						response.setStatus(StatusEnum.SUCCESS);
						response.setResponse(patient);
					} else {
						removeUnregisteredUser(connection, patient);
						response.setStatus(StatusEnum.FAILED);
						response.setErrorCode("ENROLLMENT_ERROR");
						response.setErrorMessage("Somthing went wrong while enrolling! Please try again");
					}
				} else {
					connection.rollback();
					response.setStatus(StatusEnum.FAILED);
					response.setErrorCode("INTERNAL_SERVER_ERROR");
					response.setErrorMessage("Somthing went wrong while enrolling! Please try again");
				}

			} else {
				response.setStatus(StatusEnum.FAILED);
				response.setErrorCode(ispatientValidated);
				response.setErrorMessage(ispatientValidated);
			}
		} catch (TokenExpiredException e) {
			LOGGER.error(e);
			response.setStatus(StatusEnum.FAILED);
			response.setErrorCode("TOKEN_EXPIRED");
			response.setErrorMessage(e.getMessage());
			e.getStackTrace();
		} catch (Exception e) {
			LOGGER.error(e);
			try {
				connection.rollback();
				response.setStatus(StatusEnum.FAILED);
				response.setErrorCode("SOMETHING_WENT_WRONG");
				response.setErrorMessage("SOMETHING_WENT_WRONG");
			} catch (SQLException e1) {
				response.setStatus(StatusEnum.FAILED);
				LOGGER.error(e1);
			}
		}
		return response;
	}

	private int insertPatientAddress(Connection connection, Patient patient) throws SQLException {
		int count = 0;
		PreparedStatement preparedStatement = null;
		String query = "INSERT INTO PATIENT_ADDRESS (ADDRESS_ID, PATIENT_ID, ADDRESS_LINE1, ADDRESS_LINE2, CITY, COUNTRY, CAREGIVER, RELATIONSHIP_PATIENT, PRESCRIBED,LANDLINE_NUMBER,CAREGIVER_DOB,CAREGIVER_MOBILE,EIR_CODE) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
		String id = ConnectionMannager.getNextId("SELECT MAX(TO_NUMBER(ADDRESS_ID)) FROM PATIENT_ADDRESS");
		if (id != null && !id.trim().isEmpty()) {
			Date date2 =null;
			preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, id);
			preparedStatement.setString(2, patient.getId());
			preparedStatement.setString(3, patient.getAddress());
			preparedStatement.setString(4, patient.getAddress2());
			preparedStatement.setString(5, patient.getCity());
			preparedStatement.setString(6, patient.getCountry());
			preparedStatement.setString(7, patient.getCareGiver());
			preparedStatement.setString(8, patient.getRelationshipPatient());
			preparedStatement.setString(9, patient.getPrescribed());
			preparedStatement.setString(10, patient.getLandline_Number());
			if(patient.getCareGiverDOB()!=null &&(!patient.getCareGiverDOB().equalsIgnoreCase("")))
			{
				preparedStatement.setDate(11, Date.valueOf(patient.getCareGiverDOB()));
			}else {
				preparedStatement.setDate(11, date2);	
			}
			preparedStatement.setString(12, patient.getCareGiverMobile());
			preparedStatement.setString(13, patient.getEirCode());
			count = preparedStatement.executeUpdate();
		}
		if (count > 0) {
			LOGGER.debug("Inserted Patient Address ID:" + id);
		}
		return count;
	}

	private void removeUnregisteredUser(Connection connection, Patient patient) {
		Statement statement = null;
		String queryForPatientChannel = "DELETE FROM PATIENT_CHANNEL  WHERE PATIENT_ID= '" + patient.getId() + "')";
		String queryForPatientPhyscian = "DELETE FROM PATIENT_PHYSICIAN  WHERE PATIENT_ID= '" + patient.getId() + "')";
		String queryForPatientPhone = "DELETE FROM PATIENT_PHONE  WHERE PATIENT_ID= '" + patient.getId() + "')";
		String queryForPatient = "DELETE FROM PATIENT  WHERE PATIENT_ID= '" + patient.getId() + "')";
		try {
			statement = connection.createStatement();
			statement.addBatch(queryForPatientChannel);
			statement.addBatch(queryForPatientPhyscian);
			statement.addBatch(queryForPatientPhone);
			statement.addBatch(queryForPatient);
			statement.executeBatch();
		} catch (SQLException e) {
			LOGGER.error(e);
			e.printStackTrace();
		}

	}

	private int insertPatientChannel(Connection connection, Patient patient) throws SQLException {
		int[] count;
		List<String> channelList = Stream.of(patient.getChannelPreference().split(",")).collect(Collectors.toList());
		PreparedStatement preparedStatement = null;
		String query = "INSERT INTO PATIENT_CHANNEL (PATIENT_ID, CHANNEL_ID, PREFERENCE_STATUS, DATA_AVAILABLE) VALUES(?,?,?,?)";
		preparedStatement = connection.prepareStatement(query);
		for (String channel : channelList) {
			preparedStatement.setString(1, patient.getId());
			preparedStatement.setInt(2, Integer.parseInt(channel));
			preparedStatement.setInt(3, 1);
			preparedStatement.setInt(4, 1);
			preparedStatement.addBatch();
		}
		count = preparedStatement.executeBatch();
		LOGGER.debug("Inserted Patient ID:" + patient.getId() + "Channels :" + patient.getChannelPreference());
		return count.length;
	}

	@Override
	public CommonResponse mobileVerify(Patient patient, String token) {
		LOGGER.debug("Verifying the phone number :" + patient.getMobile());
		CommonResponse commonResponse = new CommonResponse();
		try {
			authService.parseToken(token);
			Patient patientData = getPatientDetailsByPhone(patient);
			if (patientData != null) {
				commonResponse.setStatus(StatusEnum.SUCCESS);
				commonResponse.setResponse(patientData);
			} else {
				commonResponse.setStatus(StatusEnum.FAILED);
				commonResponse.setErrorCode("ERROR");
				commonResponse.setErrorMessage("Patient details not found!");
			}
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			e.getStackTrace();
			commonResponse.setStatus(StatusEnum.FAILED);
			commonResponse.setErrorCode("ERROR");
			commonResponse.setErrorMessage("Error: Something went wrong");
		}
		return commonResponse;
	}

	@Override
	public CommonResponse search(SearchVO search, String token) {
		LOGGER.debug("Searching patient details with search_key :" + search.getSearch_key());
		CommonResponse commonResponse = new CommonResponse();
		try {
			authService.parseToken(token);
			String searchKey = search.getSearch_key();
			String searchValue = search.getSearch_value();
			List<Patient> patientList = new ArrayList<>();
			if (searchKey != null && searchValue != null) {
				searchKey = searchKey.trim().toUpperCase();
				searchValue = searchValue.trim().toUpperCase();
				switch (searchKey) {
				case "MOBILE":
					patientList = getPatientDetailsByPhone(searchValue);
					break;
				case "NAME":
					patientList = getPatientDetailsByName(searchValue);
					break;
				case "ADDRESS":
					patientList = getPatientDetailsByAddress(searchValue);
					break;
				case "CITY":
					patientList = getPatientDetailsByCity(searchValue);
					break;
				case "LANDLINE":
					patientList = getPatientDetailsByLandLine(searchValue);
					break;
				default:
					break;
				}
				commonResponse.setStatus(StatusEnum.SUCCESS);
				commonResponse.setResponse(patientList);
			}
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			e.getStackTrace();
			commonResponse.setStatus(StatusEnum.FAILED);
			commonResponse.setErrorCode("ERROR");
			commonResponse.setErrorMessage("Error: Something went wrong");
		}
		return commonResponse;
	}

	@Override
	public CommonResponse unsubscribe(Patient patient, String token) {
		LOGGER.info("Unsubscribing the patient from campaign :" + patient.getCampaignId());
		CommonResponse commonResponse = new CommonResponse();
		try {
			authService.parseToken(token);
			if (isPatientActive(patient)) {
				addUnsubscribeList(patient);
				commonResponse.setStatus(StatusEnum.SUCCESS);
				commonResponse.setResponse("Unsubscribed successfully");
			} else {
				commonResponse.setStatus(StatusEnum.FAILED);
				commonResponse.setErrorCode(StatusEnum.FAILED.toString());
				commonResponse
						.setErrorMessage("Patient already unsubscribed to the campain / not active in the campain");
			}
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			e.getStackTrace();
			commonResponse.setStatus(StatusEnum.FAILED);
			commonResponse.setErrorCode("ERROR");
			commonResponse.setErrorMessage("Error: Something went wrong");
		}
		return null;
	}

	private void addUnsubscribeList(Patient patient) throws SQLException {
		Connection connection = ConnectionMannager.GetConnection();
		PreparedStatement preparedStatement = null;
		String id = ConnectionMannager.getNextId("SELECT MAX(TO_NUMBER(ID)) FROM PATIENT_CHANNEL_OPTOUT");
		String emailChannelOptoutQuery = "INSERT INTO PATIENT_CHANNEL_OPTOUT (ID, CHANNEL_NAME, STATUS, PATIENT_ID, CAMPAIGN_ID) VALUES(?,?,?,?,?)";
		String patientEmailQuery = "UPDATE PATIENT_PHONE SET IS_ACTIVE = 0 WHERE PATIENT_ID = ?";
		preparedStatement = connection.prepareStatement(emailChannelOptoutQuery);
		preparedStatement.setString(1, id);
		preparedStatement.setString(2, "SMS");
		preparedStatement.setInt(3, 1);
		preparedStatement.setString(4, patient.getId());
		preparedStatement.setString(5, patient.getCampaignId());
		preparedStatement.executeUpdate();

		preparedStatement = connection.prepareStatement(patientEmailQuery);
		preparedStatement.setString(1, patient.getId());
		preparedStatement.executeUpdate();

		LOGGER.debug("Inserted SMS PATIENT_CHANNEL_OPTOUT ID:" + id);
		connection.close();

	}

	private boolean isPatientActive(Patient patient) throws SQLException {
		LOGGER.debug("Checking is patient active for patitentId :" + patient.getId() + " campainId :"
				+ patient.getCampaignId());
		boolean status = false;
		Connection connection = ConnectionMannager.GetConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		String activeQuery = "SELECT PATIENT_ID FROM CAMPAIGN_PATIENT WHERE CAMPAIGN_ID=" + patient.getCampaignId()
				+ " AND PATIENT_ID IN (SELECT PATIENT_ID FROM PATIENT_EMAIL WHERE PATIENT_EMAIL.IS_ACTIVE = 1 AND PATIENT_EMAIL.PATIENT_ID = '"
				+ patient.getId() + "')";
		statement = connection.createStatement();
		resultSet = statement.executeQuery(activeQuery);
		while (resultSet.next()) {
			status = true;
		}
		connection.close();
		LOGGER.debug("Patient active status :" + status);
		return status;
	}

	private List<Patient> getPatientDetailsByName(String searchValue) throws SQLException {
		Patient patient = null;
		List<Patient> patientList = new ArrayList<>();
		Connection connection = ConnectionMannager.GetConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(
				"SELECT pa.patient_id, pa.first_name, pa.last_name, (SELECT TO_CHAR(pa.date_of_birth, 'DD-MM-YYYY') FROM dual) date_of_birth, pa_ad.country, pa_ph.phone_number, ");
		queryBuilder.append("CONCAT(CONCAT(pa_ad.address_line1, ' '), pa_ad.address_line2) address,pa_ad.address_line2,pa_ad.city,pa_ad.landline_number, ");
		queryBuilder.append("(SELECT LISTAGG(pa_ch.channel_id, ',') WITHIN GROUP (ORDER BY pa_ch.channel_id) ");
		queryBuilder.append(
				"FROM patient_channel pa_ch WHERE pa_ch.patient_id = pa.patient_id GROUP BY pa_ch.patient_id ) channels ");
		queryBuilder.append("FROM patient pa LEFT JOIN patient_address pa_ad on pa_ad.patient_id = pa.patient_id ");
		queryBuilder.append("LEFT JOIN patient_phone pa_ph on pa_ph.patient_id = pa.patient_id ");
		queryBuilder.append("WHERE upper(pa.first_name) like '%" + searchValue + "%' OR upper(last_name) like '%"
				+ searchValue + "%'");
		String query = queryBuilder.toString();
		statement = connection.createStatement();
		resultSet = statement.executeQuery(query);
		while (resultSet.next()) {
			patient = new Patient();
			patient.setFirst_name(resultSet.getString("first_name"));
			patient.setLast_name(resultSet.getString("last_name"));
			patient.setDob(resultSet.getString("date_of_birth"));
			patient.setCountry(resultSet.getString("country"));
			patient.setMobile(resultSet.getString("phone_number"));
			patient.setAddress(resultSet.getString("address"));
			patient.setAddress2(resultSet.getString("address_line2"));
			patient.setCity(resultSet.getString("city"));
			patient.setLandline_Number(resultSet.getString("landline_number"));
			patient.setChannelPreference(resultSet.getString("channels"));
			patientList.add(patient);
		}
		connection.close();
		return patientList;
	}

	private List<Patient> getPatientDetailsByPhone(String searchValue) throws SQLException {
		Patient patient = null;
		List<Patient> patientList = new ArrayList<>();
		Connection connection = ConnectionMannager.GetConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(
				"SELECT pa.patient_id, pa.first_name, pa.last_name, (SELECT TO_CHAR(pa.date_of_birth, 'DD-MM-YYYY') FROM dual) date_of_birth, pa_ad.country, pa_ph.phone_number,pa_ad.CITY,pa_ad.ADDRESS_LINE2,pa_ad.LANDLINE_NUMBER, ");
		queryBuilder.append("CONCAT(CONCAT(pa_ad.address_line1, ' '), pa_ad.address_line2) address, ");
		queryBuilder.append("(SELECT LISTAGG(pa_ch.channel_id, ',') WITHIN GROUP (ORDER BY pa_ch.channel_id) ");
		queryBuilder.append(
				"FROM patient_channel pa_ch WHERE pa_ch.patient_id = pa.patient_id GROUP BY pa_ch.patient_id ) channels ");
		queryBuilder.append("FROM patient pa LEFT JOIN patient_address pa_ad on pa_ad.patient_id = pa.patient_id ");
		queryBuilder.append("LEFT JOIN patient_phone pa_ph on pa_ph.patient_id = pa.patient_id ");
		queryBuilder.append("WHERE pa_ph.phone_number like '%" + searchValue + "%'");
		queryBuilder.append("OR pa_ad.LANDLINE_NUMBER LIKE '%" + searchValue + "%'");
		String query = queryBuilder.toString();
		statement = connection.createStatement();
		resultSet = statement.executeQuery(query);
		while (resultSet.next()) {
			patient = new Patient();
			patient.setFirst_name(resultSet.getString("first_name"));
			patient.setLast_name(resultSet.getString("last_name"));
			patient.setDob(resultSet.getString("date_of_birth"));
			patient.setCountry(resultSet.getString("country"));
			patient.setMobile(resultSet.getString("phone_number"));
			patient.setAddress(resultSet.getString("address"));
			patient.setAddress2(resultSet.getString("ADDRESS_LINE2"));
			patient.setCity(resultSet.getString("CITY"));
			patient.setLandline_Number(resultSet.getString("LANDLINE_NUMBER"));
			patient.setChannelPreference(resultSet.getString("channels"));
			patientList.add(patient);
		}
		connection.close();
		return patientList;
	}

	private int insertPatientPhysician(Connection connection, Patient patient) throws SQLException {
		int count = 0;
		PreparedStatement preparedStatement = null;
		String query = "INSERT INTO PATIENT_PHYSICIAN (ID, PATIENT_ID, PHYSICIAN_ID) VALUES(?,?,?)";
		String id = ConnectionMannager.getNextId("SELECT MAX(ID) FROM PATIENT_PHYSICIAN");
		if (id != null && !id.trim().isEmpty()) {
			preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, id);
			preparedStatement.setString(2, patient.getId());
			preparedStatement.setString(3, prolioPhysicianId);
			count = preparedStatement.executeUpdate();
		}
		if (count > 0) {
			LOGGER.debug("Inserted PatientPhysician ID:" + id);
		}
		return count;
	}

	private int insertPatientMobile(Connection connection, Patient patient) throws SQLException {
		int count = 0;
		PreparedStatement preparedStatement = null;
		String query = "insert into PATIENT_PHONE (PATIENT_ID, PHONE_NUMBER, ID) values(?,?,?)";
		String id = ConnectionMannager.getNextId("SELECT MAX(ID) FROM PATIENT_PHONE");
		if (id != null && !id.trim().isEmpty()) {
			preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, patient.getId());
			preparedStatement.setString(2, patient.getMobile());
			preparedStatement.setString(3, id);
			count = preparedStatement.executeUpdate();
		}
		if (count > 0) {
			LOGGER.debug("Inserted PatientMobile ID:" + id);
		}
		return count;
	}

	private int insertPatient(Connection connection, Patient patient) throws SQLException {
		int count = 0;
		PreparedStatement preparedStatement = null;
		String query = "INSERT INTO PATIENT (PATIENT_ID, FIRST_NAME, LAST_NAME, DATE_OF_BIRTH, CONCENT) VALUES(?,?,?,?,?)";
		String id = ConnectionMannager.getNextId("SELECT MAX(TO_NUMBER(PATIENT_ID)) FROM PATIENT");
		if (id != null && !id.trim().isEmpty()) {
			patient.setId(id);
			preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, patient.getId());
			preparedStatement.setString(2, patient.getFirst_name());
			preparedStatement.setString(3, patient.getLast_name());
			preparedStatement.setDate(4, Date.valueOf(patient.getDob()));
			preparedStatement.setInt(5, patient.getConcent());
			count = preparedStatement.executeUpdate();
		}
		if (count > 0) {
			LOGGER.debug("Inserted Patient ID:" + id);
		}
		return count;
	}

	private String patientValidation(Patient patient) {
		String validationCode = "";
		if (patient == null) {
			validationCode = "Please Enter Patient Details";
		} else if (patient.getFirst_name() == null) {
			validationCode = "Please enter Valid First Name";
		} else if (patient.getFirst_name() != null && patient.getFirst_name().trim().isEmpty()) {
			validationCode = "Please enter Valid First Name";
		} else if (patient.getMobile() == null && patient.getLandline_Number() == null) {
			validationCode = "Please enter Valid Mobile or Landline Number";
		} else if (patient.getMobile() != null && patient.getMobile().trim().isEmpty() && patient.getLandline_Number() != null && patient.getLandline_Number().trim().isEmpty()) {
			validationCode = "Please enter Valid Mobile or Landline Number";
		} else if (patient.getConcent() != 1) {
			validationCode = "You need select the communication agreement";
		} else if(!isuniquePatientByLandlineNumber(patient)) {
			validationCode = "Your contact First Name and DOB and Landline is already registered. Please enter a different one.";
		}else if(!isuniquePatientByNameDobMobile(patient)){
			validationCode = "Your contact First Name and DOB and Mobile is already registered. Please enter a different one.";
		}else {
			validationCode = "VALID";
		}
		return validationCode;
	}

	private boolean isuniquePatient(Patient patient) {
		Connection connection = ConnectionMannager.GetConnection();
		boolean status = true;
		Statement statement = null;
		ResultSet resultSetForPhone = null;
		String queryForPhone = "select PATIENT_ID from patient_phone WHERE phone_number = '" + patient.getMobile()
				+ "'";
		try {
			statement = connection.createStatement();
			resultSetForPhone = statement.executeQuery(queryForPhone);
			while (resultSetForPhone.next()) {
				status = false;
			}
			connection.close();
		} catch (SQLException e) {
			LOGGER.error(e);
			e.printStackTrace();
		}
		return status;
	}

	private Patient getPatientDetailsByPhone(Patient patient) {
		Connection connection = ConnectionMannager.GetConnection();
		Patient patientData = null;
		Statement statement = null;
		ResultSet resultSetForPhone = null;
		String queryForPhone = "select PATIENT_ID from patient_phone WHERE phone_number = '" + patient.getMobile()+ "'";
		try {
			statement = connection.createStatement();
			resultSetForPhone = statement.executeQuery(queryForPhone);
			while (resultSetForPhone.next()) {
				patientData = new Patient();
				patientData.setId(resultSetForPhone.getString("PATIENT_ID"));
			}
			if(patientData==null)
			{
				queryForPhone="SELECT PATIENT_ID FROM PATIENT_ADDRESS pa WHERE landline_number='"+patient.getMobile()+"'";
				resultSetForPhone = statement.executeQuery(queryForPhone);
				while (resultSetForPhone.next()) {
					patientData = new Patient();
					patientData.setId(resultSetForPhone.getString("PATIENT_ID"));
					}
			}
			connection.close();
		} catch (SQLException e) {
			LOGGER.error(e);
			e.printStackTrace();
		}
		return patientData;
	}
	private List<Patient> getPatientDetailsByAddress(String searchValue) throws SQLException {
		Patient patient = null;
		List<Patient> patientList = new ArrayList<>();
		Connection connection = ConnectionMannager.GetConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(
				"SELECT pa.patient_id, pa.first_name, pa.last_name, (SELECT TO_CHAR(pa.date_of_birth, 'DD-MM-YYYY') FROM dual) date_of_birth, pa_ad.country, pa_ph.phone_number, pa_ad.address_line2,pa_ad.CITY,pa_ad.LANDLINE_NUMBER ,");
		queryBuilder.append("CONCAT(CONCAT(pa_ad.address_line1, ' '), pa_ad.address_line2) address, ");
		queryBuilder.append("(SELECT LISTAGG(pa_ch.channel_id, ',') WITHIN GROUP (ORDER BY pa_ch.channel_id) ");
		queryBuilder.append(
				"FROM patient_channel pa_ch WHERE pa_ch.patient_id = pa.patient_id GROUP BY pa_ch.patient_id ) channels ");
		queryBuilder.append("FROM patient pa LEFT JOIN patient_address pa_ad on pa_ad.patient_id = pa.patient_id ");
		queryBuilder.append("LEFT JOIN patient_phone pa_ph on pa_ph.patient_id = pa.patient_id ");
		queryBuilder.append("WHERE upper(pa_ad.ADDRESS_LINE1) like '%" + searchValue + "%' OR upper(ADDRESS_LINE2) like '%"
				+ searchValue + "%'");
		String query = queryBuilder.toString();
		statement = connection.createStatement();
		resultSet = statement.executeQuery(query);
		while (resultSet.next()) {
			patient = new Patient();
			patient.setFirst_name(resultSet.getString("first_name"));
			patient.setLast_name(resultSet.getString("last_name"));
			patient.setDob(resultSet.getString("date_of_birth"));
			patient.setCountry(resultSet.getString("country"));
			patient.setMobile(resultSet.getString("phone_number"));
			patient.setAddress(resultSet.getString("address"));
			patient.setCity(resultSet.getString("CITY"));
			patient.setAddress2(resultSet.getString("address_line2"));
			patient.setLandline_Number(resultSet.getString("LANDLINE_NUMBER"));
			patient.setChannelPreference(resultSet.getString("channels"));
			patientList.add(patient);
		}
		connection.close();
		return patientList;
	}
	private List<Patient> getPatientDetailsByCity(String searchValue) throws SQLException {
		Patient patient = null;
		List<Patient> patientList = new ArrayList<>();
		Connection connection = ConnectionMannager.GetConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(
				"SELECT pa.patient_id, pa.first_name, pa.last_name, (SELECT TO_CHAR(pa.date_of_birth, 'DD-MM-YYYY') FROM dual) date_of_birth, pa_ad.country, pa_ph.phone_number, ");
		queryBuilder.append("CONCAT(CONCAT(pa_ad.address_line1, ' '), pa_ad.address_line2) address,pa_ad.CITY,pa_ad.ADDRESS_LINE2,pa_ad.LANDLINE_NUMBER, ");
		queryBuilder.append("(SELECT LISTAGG(pa_ch.channel_id, ',') WITHIN GROUP (ORDER BY pa_ch.channel_id) ");
		queryBuilder.append(
				"FROM patient_channel pa_ch WHERE pa_ch.patient_id = pa.patient_id GROUP BY pa_ch.patient_id ) channels ");
		queryBuilder.append("FROM patient pa LEFT JOIN patient_address pa_ad on pa_ad.patient_id = pa.patient_id ");
		queryBuilder.append("LEFT JOIN patient_phone pa_ph on pa_ph.patient_id = pa.patient_id ");
		queryBuilder.append("WHERE upper(pa_ad.CITY) like '%" + searchValue+"%'");
		String query = queryBuilder.toString();
		statement = connection.createStatement();
		resultSet = statement.executeQuery(query);
		while (resultSet.next()) {
			patient = new Patient();
			patient.setFirst_name(resultSet.getString("first_name"));
			patient.setLast_name(resultSet.getString("last_name"));
			patient.setDob(resultSet.getString("date_of_birth"));
			patient.setCountry(resultSet.getString("country"));
			patient.setMobile(resultSet.getString("phone_number"));
			patient.setAddress(resultSet.getString("address"));
			patient.setCity(resultSet.getString("CITY"));
			patient.setAddress2(resultSet.getString("ADDRESS_LINE2"));
			patient.setLandline_Number(resultSet.getString("LANDLINE_NUMBER"));
			patient.setChannelPreference(resultSet.getString("channels"));
			patientList.add(patient);
		}
		connection.close();
		return patientList;
	}
	private List<Patient> getPatientDetailsByLandLine(String searchValue) throws SQLException {
		Patient patient = null;
		List<Patient> patientList = new ArrayList<>();
		Connection connection = ConnectionMannager.GetConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(
				"SELECT pa.patient_id, pa.first_name, pa.last_name, (SELECT TO_CHAR(pa.date_of_birth, 'DD-MM-YYYY') FROM dual) date_of_birth, pa_ad.country, pa_ph.phone_number, ");
		queryBuilder.append("CONCAT(CONCAT(pa_ad.address_line1, ' '), pa_ad.address_line2) address,pa_ad.CITY,pa_ad.LANDLINE_NUMBER,pa_ad.address_line2, ");
		queryBuilder.append("(SELECT LISTAGG(pa_ch.channel_id, ',') WITHIN GROUP (ORDER BY pa_ch.channel_id) ");
		queryBuilder.append(
				"FROM patient_channel pa_ch WHERE pa_ch.patient_id = pa.patient_id GROUP BY pa_ch.patient_id ) channels ");
		queryBuilder.append("FROM patient pa LEFT JOIN patient_address pa_ad on pa_ad.patient_id = pa.patient_id ");
		queryBuilder.append("LEFT JOIN patient_phone pa_ph on pa_ph.patient_id = pa.patient_id ");
		queryBuilder.append("WHERE upper(pa_ad.LANDLINE_NUMBER) like '%" + searchValue+"%'");
		String query = queryBuilder.toString();
		statement = connection.createStatement();
		resultSet = statement.executeQuery(query);
		while (resultSet.next()) {
			patient = new Patient();
			patient.setFirst_name(resultSet.getString("first_name"));
			patient.setLast_name(resultSet.getString("last_name"));
			patient.setDob(resultSet.getString("date_of_birth"));
			patient.setCountry(resultSet.getString("country"));
			patient.setMobile(resultSet.getString("phone_number"));
			patient.setAddress(resultSet.getString("address"));
			patient.setAddress2(resultSet.getString("address_line2"));
			patient.setCity(resultSet.getString("CITY"));
			patient.setLandline_Number(resultSet.getString("LANDLINE_NUMBER"));
			patient.setChannelPreference(resultSet.getString("channels"));
			patientList.add(patient);
		}
		connection.close();
		return patientList;
	}
	private boolean isuniquePatientByNameDobMobile(Patient patient) {
		Connection connection = ConnectionMannager.GetConnection();
		boolean status = true;
		Statement statement = null;
		ResultSet resultSetForPhone = null;
		
		String queryForPhone = "SELECT pa.PATIENT_ID,pa.FIRST_NAME,pa.LAST_NAME FROM PATIENT pa,PATIENT_PHONE pp WHERE pa.PATIENT_ID=pp.PATIENT_ID AND pp.PHONE_NUMBER = '" + patient.getMobile()+ "' AND pa.FIRST_NAME='"+patient.getFirst_name()+"' AND TO_CHAR(DATE_OF_BIRTH, 'YYYY-mm-DD')='"+patient.getDob()+"'";
		try {
			statement = connection.createStatement();
			resultSetForPhone = statement.executeQuery(queryForPhone);
			while (resultSetForPhone.next()) {
				status = false;
			}
			connection.close();
		} catch (SQLException e) {
			LOGGER.error(e);
			e.printStackTrace();
		}
		return status;
	}
	private boolean isuniquePatientByLandlineNumber(Patient patient) {
		Connection connection = ConnectionMannager.GetConnection();
		boolean status = true;
		Statement statement = null;
		ResultSet resultSetForPhone = null;
		
		String queryForPhone = "SELECT pa.PATIENT_ID,pa.FIRST_NAME,pa.LAST_NAME,pa_add.LANDLINE_NUMBER FROM PATIENT pa,PATIENT_PHONE pp,PATIENT_ADDRESS pa_add  WHERE pa.PATIENT_ID=pp.PATIENT_ID AND pa.PATIENT_ID=pa_add.PATIENT_ID AND pa_add.LANDLINE_NUMBER = '" + patient.getLandline_Number()+ "' AND pa.FIRST_NAME='"+patient.getFirst_name()+"' AND TO_CHAR(DATE_OF_BIRTH, 'YYYY-mm-DD')='"+patient.getDob()+"'";
		try {
			statement = connection.createStatement();
			resultSetForPhone = statement.executeQuery(queryForPhone);
			while (resultSetForPhone.next()) {
				status = false;
			}
			connection.close();
		} catch (SQLException e) {
			LOGGER.error(e);
			e.printStackTrace();
		}
		return status;
	}
}