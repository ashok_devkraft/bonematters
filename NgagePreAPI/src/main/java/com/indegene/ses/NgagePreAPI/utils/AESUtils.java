package com.indegene.ses.NgagePreAPI.utils;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

/***
 * 
 * @author vinaya.math
 *
 */
public class AESUtils {
	private static final Logger LOGGER = Logger.getLogger(AESUtils.class);
	private static final String key = "5TGB&YHN7UJM(IK<5TGB&YHN7UJM(IK<";
	private static final String initVector = "!QAZ2WSX#EDC4RFV";

	public static String decryption(String encrypted) throws Exception {
		LOGGER.debug("Started decryption :" + encrypted);
		String response = null;
		IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
		SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
		byte[] original = cipher.doFinal(Base64.decodeBase64(encrypted));

		String decryptedString = new String(original);

		char tempChar = ' ';
		int tempCharASCII = 0;
		StringBuilder stringBuilder = new StringBuilder();
		for (int j = 0; j < decryptedString.length(); j++) {
			tempChar = decryptedString.charAt(j);
			tempCharASCII = (int) tempChar;
			if (tempCharASCII > 0) {
				stringBuilder.append(decryptedString.charAt(j));
			}
		}
		response = stringBuilder.toString();
		LOGGER.debug("Completed decryption :" + response);
		return response;
	}
}
