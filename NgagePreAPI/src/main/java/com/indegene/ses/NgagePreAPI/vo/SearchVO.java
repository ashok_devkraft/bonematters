package com.indegene.ses.NgagePreAPI.vo;

public class SearchVO {

	private String search_key;
	private String search_value;

	public SearchVO() {

	}

	public SearchVO(String search_key, String search_value) {
		this.search_key = search_key;
		this.search_value = search_value;
	}

	public String getSearch_key() {
		return search_key;
	}

	public void setSearch_key(String search_key) {
		this.search_key = search_key;
	}

	public String getSearch_value() {
		return search_value;
	}

	public void setSearch_value(String search_value) {
		this.search_value = search_value;
	}

}
