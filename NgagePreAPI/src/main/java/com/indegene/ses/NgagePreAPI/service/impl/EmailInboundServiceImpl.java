package com.indegene.ses.NgagePreAPI.service.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Base64;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.indegene.ses.NgagePreAPI.common.CommonResponse;
import com.indegene.ses.NgagePreAPI.common.ConnectionMannager;
import com.indegene.ses.NgagePreAPI.enums.StatusEnum;
import com.indegene.ses.NgagePreAPI.service.EmailInboundService;
import com.indegene.ses.NgagePreAPI.vo.SendGridEventVO;

@Service
public class EmailInboundServiceImpl implements EmailInboundService {

	private static final Logger LOGGER = Logger.getLogger(EmailInboundServiceImpl.class);

	@Override
	public void processRequest(String body) {
		LOGGER.debug("Processing the request : " + body);
		SendGridEventVO sendGridEventVO = null;
		try {
			if (body != null && !body.trim().isEmpty()) {
				sendGridEventVO = fromJson(body);
			}
			// TODO need to check is active in if case based on ngagEmailUniqueId
			if (sendGridEventVO != null && sendGridEventVO.getNgagEmailUniqueId() != null
					&& !sendGridEventVO.getNgagEmailUniqueId().trim().isEmpty()) {

				updateEmailReportsData(sendGridEventVO);

				if (sendGridEventVO.getEvent().equalsIgnoreCase("open")) {
					updateOpenStatus(sendGridEventVO.getNgagEmailUniqueId().trim());
				}
			}
		} catch (Exception e) {
			LOGGER.error(e);
			e.printStackTrace();
		}
	}

	private void updateEmailReportsData(SendGridEventVO sendGridEventVO) throws SQLException {
		LOGGER.debug("Updating the Email Reports Data");
		String id = ConnectionMannager.getNextId("SELECT ID FROM SENDGRID_EMAIL_STATUS");
		String query = "INSERT INTO SENDGRID_EMAIL_STATUS (ID, EMAIL, NGAGE_EMAIL_QUEUE_ID, STATUS) VALUES(?,?,?,?)";
		Connection connection = ConnectionMannager.GetConnection();
		PreparedStatement preparedStatement = null;
		preparedStatement = connection.prepareStatement(query);
		preparedStatement.setInt(1, id != null ? Integer.parseInt(id) : 1);
		preparedStatement.setString(2, sendGridEventVO.getEmail());
		preparedStatement.setString(3, sendGridEventVO.getNgagEmailUniqueId());
		preparedStatement.setString(4, sendGridEventVO.getEvent());
		int count = preparedStatement.executeUpdate();
		if (count > 0) {
			LOGGER.debug("Inserted SENDGRID_EMAIL_STATUS with ID:" + id);
		}
		connection.close();
	}

	private void updateOpenStatus(String messageId) throws SQLException {
		Connection connection = ConnectionMannager.GetConnection();
		Statement statement = null;
		String query = "UPDATE EMAIL_QUEUE SET EMAIL_STATUS = 'OPENED' WHERE ID = '" + messageId + "'";
		statement = connection.createStatement();
		int status = statement.executeUpdate(query);
		connection.close();
		LOGGER.debug("messageId :" + messageId + " update status :" + (status == 1 ? "Updated" : "Failed"));
	}

	@Override
	public CommonResponse unsubscribe(String link) {
		LOGGER.debug("Started processing the unsubscribe");
		CommonResponse response = new CommonResponse();
		try {
			if (link != null && !link.trim().isEmpty()) {
				String[] ids = parseLink(link);
				if (ids != null && ids.length == 2) {
					if (isPatientActive(ids[0], ids[1]) && (!isUnsubscribed(ids[0], ids[1]))) {
						addUnsubscribeList(ids[0], ids[1]);
						response.setStatus(StatusEnum.SUCCESS);
						response.setResponse("Unsubscribed successfully");
					} else {
						response.setStatus(StatusEnum.FAILED);
						response.setErrorCode(StatusEnum.FAILED.toString());
						response.setErrorMessage(
								"Patient already unsubscribed to the campain / not active in the campain");
					}
				} else {
					LOGGER.error("pars error");
					response.setStatus(StatusEnum.FAILED);
					response.setErrorCode(StatusEnum.FAILED.toString());
					response.setErrorMessage(StatusEnum.FAILED.toString());
				}
			}
		} catch (Exception e) {
			response.setStatus(StatusEnum.FAILED);
			response.setErrorCode(StatusEnum.FAILED.toString());
			response.setErrorMessage(StatusEnum.FAILED.toString());
			LOGGER.error(e);
			e.printStackTrace();
		}
		return response;
	}

	/**
	 * 
	 * This is the method to check weather given patient is active or not in given
	 * campaign
	 * 
	 * @param patitentId
	 * @param campainId
	 * @return <b>true</b> if patient is still active in the campaign
	 * @throws SQLException
	 */
	private boolean isPatientActive(String patitentId, String campainId) throws SQLException {
		LOGGER.debug("Checking is patient active for patitentId :" + patitentId + " campainId :" + campainId);
		boolean status = false;
		Connection connection = ConnectionMannager.GetConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		String activeQuery = "SELECT PATIENT_ID FROM CAMPAIGN_PATIENT WHERE CAMPAIGN_ID=" + campainId
				+ " AND PATIENT_ID IN (SELECT PATIENT_ID FROM PATIENT_EMAIL WHERE PATIENT_EMAIL.IS_ACTIVE = 1 AND PATIENT_EMAIL.PATIENT_ID = '"
				+ patitentId + "')";
		statement = connection.createStatement();
		resultSet = statement.executeQuery(activeQuery);
		while (resultSet.next()) {
			status = true;
		}
		connection.close();
		LOGGER.debug("Patient active status :" + status);
		return status;
	}

	private boolean isUnsubscribed(String patitentId, String campainId) throws SQLException {
		LOGGER.debug("Checking is patient unsubscribed for patitentId :" + patitentId + " campainId :" + campainId);
		boolean status = false;
		Connection connection = ConnectionMannager.GetConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		String query = "SELECT ID FROM PATIENT_CHANNEL_OPTOUT WHERE CHANNEL_NAME = 'EMAIL' AND PATIENT_ID = '"
				+ patitentId + "' AND CAMPAIGN_ID = " + campainId;
		statement = connection.createStatement();
		resultSet = statement.executeQuery(query);
		while (resultSet.next()) {
			status = true;
		}
		connection.close();
		LOGGER.debug("Patient unsubscribed status :" + status);
		return status;
	}

	private void addUnsubscribeList(String patientId, String campainId) throws SQLException {
		Connection connection = ConnectionMannager.GetConnection();
		PreparedStatement preparedStatement = null;
		String id = ConnectionMannager.getNextId("SELECT MAX(TO_NUMBER(ID)) FROM PATIENT_CHANNEL_OPTOUT");
		String emailChannelOptoutQuery = "INSERT INTO PATIENT_CHANNEL_OPTOUT (ID, CHANNEL_NAME, STATUS, PATIENT_ID, CAMPAIGN_ID) VALUES(?,?,?,?,?)";
		String patientEmailQuery = "UPDATE PATIENT_EMAIL SET IS_ACTIVE = 0 WHERE PATIENT_ID = ?";
		preparedStatement = connection.prepareStatement(emailChannelOptoutQuery);
		preparedStatement.setString(1, id);
		preparedStatement.setString(2, "EMAIL");
		preparedStatement.setInt(3, 1);
		preparedStatement.setString(4, patientId);
		preparedStatement.setString(5, campainId);
		preparedStatement.executeUpdate();

		preparedStatement = connection.prepareStatement(patientEmailQuery);
		preparedStatement.setString(1, patientId);
		preparedStatement.executeUpdate();

		LOGGER.debug("Inserted EMAIL PATIENT_CHANNEL_OPTOUT ID:" + id);
		connection.close();
	}

	private String[] parseLink(String unsubscribeLink) throws UnsupportedEncodingException {
		String base64Decode = new String(Base64.getDecoder().decode(URLDecoder.decode(unsubscribeLink)), "utf-8");
		String[] ids = base64Decode.split("###");
		return ids;
	}

	private SendGridEventVO fromJson(String json) {
		return new Gson().fromJson(json, SendGridEventVO[].class)[0];
	}

}
