package com.indegene.ses.NgagePreAPI.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.indegene.ses.NgagePreAPI.common.CommonResponse;
import com.indegene.ses.NgagePreAPI.service.BatchCodeService;
import com.indegene.ses.NgagePreAPI.vo.BatchCodeParam;

@RestController
@RequestMapping("/batchcode")
public class BatchCodeController {
	private static final Logger LOGGER = Logger.getLogger(BatchCodeController.class);

	@Autowired
	public BatchCodeService batchCodeService;

	@PostMapping("/details")
	public CommonResponse getByCode(@RequestBody BatchCodeParam batchCode, @RequestHeader("Authorization") String token) {
		LOGGER.info("Started getting batchcode details by code:" + batchCode);
		CommonResponse response = batchCodeService.getBatchCodeDetailsByCode(batchCode, token);
		LOGGER.info("Completed getting batchcode details with Status:" + response.getStatus());
		return response;
	}
}
