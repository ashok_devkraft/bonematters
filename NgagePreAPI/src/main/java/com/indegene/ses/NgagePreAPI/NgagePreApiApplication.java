package com.indegene.ses.NgagePreAPI;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class NgagePreApiApplication extends SpringBootServletInitializer {
	private static final Logger LOGGER = Logger.getLogger(NgagePreApiApplication.class);

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(NgagePreApiApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(NgagePreApiApplication.class, args);
	}

	@GetMapping(value = "/")
	public String hello() {
		LOGGER.info("Started testing root URL");
		return "Hello World from NgagePreAPI";
	}

}
