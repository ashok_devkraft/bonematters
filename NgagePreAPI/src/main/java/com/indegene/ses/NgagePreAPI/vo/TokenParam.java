package com.indegene.ses.NgagePreAPI.vo;

public class TokenParam {
	private String client_id;
	private String client_secret;
	private String scope;

	public TokenParam() {

	}

	public TokenParam(String client_id, String client_secret, String scope) {
		this.client_id = client_id;
		this.client_secret = client_secret;
		this.scope = scope;
	}

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public String getClient_secret() {
		return client_secret;
	}

	public void setClient_secret(String client_secret) {
		this.client_secret = client_secret;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

}
