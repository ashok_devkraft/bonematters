package com.indegene.ses.NgagePreAPI.common;

import com.indegene.ses.NgagePreAPI.enums.StatusEnum;

public class CommonResponse {
	private StatusEnum status;
	private Object response;
	private String errorCode;
	private String errorMessage;

	public CommonResponse() {

	}

	public CommonResponse(StatusEnum status, Object response, String errorCode, String errorMessage) {
		this.status = status;
		this.response = response;
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	public StatusEnum getStatus() {
		return status;
	}

	public void setStatus(StatusEnum status) {
		this.status = status;
	}

	public Object getResponse() {
		return response;
	}

	public void setResponse(Object response) {
		this.response = response;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
