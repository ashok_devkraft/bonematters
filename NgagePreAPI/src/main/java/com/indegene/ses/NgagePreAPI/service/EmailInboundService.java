package com.indegene.ses.NgagePreAPI.service;

import com.indegene.ses.NgagePreAPI.common.CommonResponse;

public interface EmailInboundService {

	public void processRequest(String body);

	public CommonResponse unsubscribe(String link);
}
