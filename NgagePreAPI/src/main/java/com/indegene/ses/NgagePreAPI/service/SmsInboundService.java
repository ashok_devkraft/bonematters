package com.indegene.ses.NgagePreAPI.service;

public interface SmsInboundService {
	public String processRequest(String body);
	public void processStatusRequest(String body);
}
