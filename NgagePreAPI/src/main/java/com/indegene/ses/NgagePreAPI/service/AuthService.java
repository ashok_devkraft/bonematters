package com.indegene.ses.NgagePreAPI.service;

import com.indegene.ses.NgagePreAPI.vo.OAuthTokenVo;
import com.indegene.ses.NgagePreAPI.vo.TokenParam;

public interface AuthService {
	public OAuthTokenVo getToken(TokenParam tokenParam);

	public void parseToken(String token);
}
