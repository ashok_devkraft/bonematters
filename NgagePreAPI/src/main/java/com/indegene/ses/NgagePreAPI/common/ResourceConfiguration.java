package com.indegene.ses.NgagePreAPI.common;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.indegene.ses.NgagePreAPI.vo.OAuthTokenVo;

public class ResourceConfiguration {
	public static void main(String[] args) throws Exception {
		System.out.println("Hi");
		new ResourceConfiguration().generateBatchcode(20, "11326");
		System.out.println("Hello");
	}

	private void name5() throws SQLException {
		System.out.println("It's me");
		Connection connection = ConnectionMannager.GetConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		String query = "SELECT * FROM BATCH_CODE";

		statement = connection.createStatement();
		resultSet = statement.executeQuery(query);
		while (resultSet.next()) {
			System.out.println(resultSet.getString(1));
		}
		connection.close();
	}

	private void name4() {
		String request_url = "https://psp-kc.indegene.com/auth/realms/ngage/protocol/openid-connect/token";
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
		map.add("client_id", "ngage-client");
		map.add("username", "test-nurseone");
		map.add("password", "nurse123@");
		map.add("grant_type", "password");
		map.add("scope", "profile email");
		HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);

		ResponseEntity<OAuthTokenVo> response = restTemplate.exchange(request_url, HttpMethod.POST, entity,
				OAuthTokenVo.class);
		String token = response.getBody().getAccess_token();
		System.out.println(token);
	}

	private void name3() {
		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		requestFactory.setConnectTimeout(3000);
		RestTemplate restTemplate = new RestTemplate(requestFactory);
		ResponseEntity<OAuthTokenVo> response = null;
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> request = new HttpEntity<String>(headers);
		String request_url = "https://psp-kc.indegene.com/auth/realms/ngage/protocol/openid-connect/token";
		request_url += "?client_id=ngage-client";
		request_url += "&username=test-nurseone";
		request_url += "&password=nurse123@";
		request_url += "&grant_type=password";
		request_url += "&scope=" + "profile email";
		// request_url = URLEncoder.encode(request_url, "utf-8");
		response = restTemplate.exchange(request_url, HttpMethod.POST, request, OAuthTokenVo.class);
		String token = response.getBody().getAccess_token();
		System.out.println(token);
		System.out.println("Hi");
	}

	private void name() throws UnsupportedEncodingException {

		String username = "test-nurseone";
		String password = "nurse123@";
		String client_id = "ngage-client";
		String scope = "";
		ResponseEntity<String> response = null;

		RestTemplate restTemplate = new RestTemplate();

		// According OAuth documentation we need to send the client id and secret key in
		// the header for authentication
		// String credentials = "javainuse:secret";
		// String encodedCredentials = new
		// String(Base64.encodeBase64(credentials.getBytes()));

		HttpHeaders headers = new HttpHeaders();
		// headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		// headers.add("Authorization", "Basic " + encodedCredentials);
		// headers.add("Content-type", "application/x-www-form-urlencoded;
		// charset=UTF-8");
		// headers.add("accept", "*/*");
		headers.add("Content-Type", "application/json");
		HttpEntity<String> request = new HttpEntity<String>(headers);

		String access_token_url = "https://psp-kc.indegene.com/auth/realms/ngage/protocol/openid-connect/token";
		access_token_url += "?client_id=" + client_id;
		// access_token_url += "&grant_type=password";
		// access_token_url += "&client_secret=" + client_secret;
		access_token_url += "&username=" + username;
		access_token_url += "&password=" + password;
		access_token_url += "&scope=profile email";
		// access_token_url += "&redirect_uri=http://localhost:8090/showEmployees";
		// access_token_url = URLEncoder.encode(access_token_url, "UTF-8");
		System.out.println(access_token_url);
		response = restTemplate.exchange(access_token_url, HttpMethod.POST, request, String.class);

		System.out.println("Access Token Response ---------" + response.getBody());
	}

	private void name2() {
		String client_id = "6cuj776l97rpqdr6fju5iaq31b";
		String client_secret = "1s9jtis4m56ls95tia4r0qk9gc67sm37i2t6bhuqjo2v819pfj1e";
		String scope = "";
		ResponseEntity<OAuthTokenVo> response = null;
		// System.out.println("Authorization Code------" + code);

		RestTemplate restTemplate = new RestTemplate();

		// According OAuth documentation we need to send the client id and secret key in
		// the header for authentication
		// String credentials = "javainuse:secret";
		// String encodedCredentials = new
		// String(Base64.encodeBase64(credentials.getBytes()));

		HttpHeaders headers = new HttpHeaders();
		// headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		// headers.add("Authorization", "Basic " + encodedCredentials);
		HttpEntity<String> request = new HttpEntity<String>(headers);

		String access_token_url = "https://api-ngage.auth.us-east-2.amazoncognito.com/oauth2/token";
		access_token_url += "?client_id=" + client_id;
		access_token_url += "&client_secret=" + client_secret;
		access_token_url += "&scope=" + "";
		access_token_url += "&grant_type=client_credentials";
		// access_token_url += "&redirect_uri=http://localhost:8090/showEmployees";

		response = restTemplate.exchange(access_token_url, HttpMethod.POST, request, OAuthTokenVo.class);
		System.out.println("Access Token Response ---------" + response.getBody().getAccess_token());
	}

	private void generateBatchcode(int total, String newCode) throws SQLException {
		Connection connection = ConnectionMannager.GetConnection();
		PreparedStatement preparedStatement = null;
		String query = "INSERT INTO BATCH_CODE (ID, CODE, STATUS) VALUES(?,?,?)";
		String id = ConnectionMannager.getNextId("SELECT MAX(TO_NUMBER(ID)) FROM BATCH_CODE");
		preparedStatement = connection.prepareStatement(query);
		String code = newCode;
		int idInt = 0;
		int codeInt = 0;
		for (int i = 0; i < total; i++) {
			preparedStatement.setString(1, id);
			preparedStatement.setString(2, code);
			preparedStatement.setInt(3, 1);
			preparedStatement.addBatch();
			idInt = Integer.parseInt(id);
			codeInt = Integer.parseInt(code);
			id = Integer.toString(++idInt);
			code = Integer.toString(++codeInt);
			System.out.println("id:" + id + " Code:" + code);
		}

		preparedStatement.executeBatch();
		connection.close();
	}

}
