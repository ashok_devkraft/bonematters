package com.indegene.ses.NgagePreAPI.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.indegene.ses.NgagePreAPI.common.CommonConstants;
import com.indegene.ses.NgagePreAPI.service.NgageService;
import com.indegene.ses.NgagePreAPI.vo.OAuthTokenVo;
import com.indegene.ses.NgagePreAPI.vo.Patient;

@Service
public class NgageServiceImpl implements NgageService {
	private static final Logger LOGGER = Logger.getLogger(NgageServiceImpl.class);

	@Value("${" + CommonConstants.NGAGE_ACCESS_TOKEN_URL + "}")
	private String access_token_url;

	@Value("${" + CommonConstants.NGAGE_ACCESS_CLIENT_NAME + "}")
	private String client_id;

	@Value("${" + CommonConstants.NGAGE_ACCESS_USERNAME + "}")
	private String username;

	@Value("${" + CommonConstants.NGAGE_ACCESS_PASSWORD + "}")
	private String password;

	@Value("${" + CommonConstants.NGAGE_PATIENT_ENROLLMENT_URL + "}")
	private String ngagePatientEnrollment_url;

	@Override
	public int campainEnroll(Patient patient) {
		LOGGER.debug("Started enrolling patient to the campain");
		int status = 0;
		try {
			String accessToken = getAccessToken();
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set("Authorization", "Bearer " + accessToken);
			/*
			 * UriComponentsBuilder builder =
			 * UriComponentsBuilder.fromHttpUrl(ngagePatientEnrollment_url).queryParam(
			 * "patientID",patient.getId()).queryParam("campaignID",
			 * patient.getCampainId());
			 */
			UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(ngagePatientEnrollment_url)
					.queryParam("patientID", patient.getId()).queryParam("campaignID", 1);
			HttpEntity<?> entity = new HttpEntity<>(headers);

			HttpEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity,
					String.class);
			LOGGER.debug(response);
			if (response != null && response.getBody().contains("\"successCount\":1")) {
				status = 1;
				LOGGER.debug("Enrollment successfully");
			}
		} catch (Exception e) {
			System.err.println(e);
			LOGGER.error(e.getMessage());
			e.getStackTrace();
		}
		return status;
	}

	private String getAccessToken() {
		LOGGER.debug("Getting token for enrollment");
		String token = null;
		ResponseEntity<OAuthTokenVo> response = null;

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
		map.add("client_id", client_id);
		map.add("username", username);
		map.add("password", password);
		map.add("grant_type", "password");
		map.add("scope", "profile email");
		HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);

		response = restTemplate.exchange(access_token_url, HttpMethod.POST, entity, OAuthTokenVo.class);
		token = response.getBody().getAccess_token();
		return token;
	}

}
