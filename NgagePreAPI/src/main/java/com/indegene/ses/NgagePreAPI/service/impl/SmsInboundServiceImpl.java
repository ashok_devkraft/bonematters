package com.indegene.ses.NgagePreAPI.service.impl;

import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.indegene.ses.NgagePreAPI.common.CommonConstants;
import com.indegene.ses.NgagePreAPI.common.ConnectionMannager;
import com.indegene.ses.NgagePreAPI.service.SmsInboundService;
import com.indegene.ses.NgagePreAPI.vo.SmsVO;

@Service
public class SmsInboundServiceImpl implements SmsInboundService {

	private static final Logger LOGGER = Logger.getLogger(SmsInboundServiceImpl.class);

	@Value("${" + CommonConstants.NGAGE_SMS_INVALID_INPUT + "}")
	private String invalidInput;

	@Override
	public String processRequest(String body) {
		LOGGER.debug("Started proceessing body:" + body);
		String replyMessage = "";
		try {
			SmsVO smsVO = parseMessage(body);
			String campainId = getCampainIdbyPhysicianPhone(smsVO.getTo()) + "";
			String patientId = getPatientIdbyPatientPhone(campainId, smsVO.getFrom().substring(3));

			if (isPatientActive(smsVO, patientId, campainId)) {
				if (smsVO.getBody().trim().equalsIgnoreCase("STOP")) {
					if (!isUnsubscribed(smsVO, patientId, campainId)) {
						replyMessage = unsubscribePatientSms(smsVO, patientId, campainId);
					}
				} else {
					replyMessage = storeReply(smsVO, patientId, campainId);
				}
			}

			if (replyMessage.trim().isEmpty()) {
				replyMessage = invalidInput;
			}
		} catch (Exception e) {
			replyMessage = invalidInput;
			LOGGER.error(e);
			e.printStackTrace();
		}
		LOGGER.debug("replyMessage:" + replyMessage);
		return replyMessage;
	}
	
	@Override
	public void processStatusRequest(String body) {
		LOGGER.debug("Started processing the status request :" + body);
		try {
			SmsVO smsVO = parseMessage(body);
			updateSmsReportsData(smsVO);
		}catch (Exception e) {
			LOGGER.error(e);
			e.printStackTrace();
		}
	}

	private void updateSmsReportsData(SmsVO smsVO) throws SQLException {
		LOGGER.debug("Updating the Sms Reports Data");
		String id = ConnectionMannager.getNextId("SELECT ID FROM TWILIO_SMS_STATUS");
		String query = "INSERT INTO TWILIO_SMS_STATUS (ID, SID, FROM_NUMBER, TO_NUMBER, STATUS) VALUES(?,?,?,?,?)";
		Connection connection = ConnectionMannager.GetConnection();
		PreparedStatement preparedStatement = null;
		preparedStatement = connection.prepareStatement(query);
		preparedStatement.setInt(1, id != null ? Integer.parseInt(id) : 1);
		preparedStatement.setString(2, smsVO.getMessageSid());
		preparedStatement.setString(3, smsVO.getFrom());
		preparedStatement.setString(4, smsVO.getTo());
		preparedStatement.setString(5, smsVO.getStatus());
		int count = preparedStatement.executeUpdate();
		if (count > 0) {
			LOGGER.debug("Inserted TWILIO_SMS_STATUS with ID:" + id);
		}
		connection.close();		
	}

	private boolean isPatientActive(SmsVO smsVO, String patientId, String campainId) throws SQLException {
		LOGGER.debug("checking is patient active");
		boolean status = false;
		Connection connection = ConnectionMannager.GetConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		String queryForPatientActive = "SELECT PATIENT_ID FROM CAMPAIGN_PATIENT WHERE CAMPAIGN_ID=" + campainId
				+ " AND PATIENT_ID IN (SELECT PATIENT_ID FROM PATIENT_PHONE WHERE IS_ACTIVE = 1 AND PHONE_NUMBER = '"
				+ smsVO.getFrom().substring(3) + "')";
		statement = connection.createStatement();
		resultSet = statement.executeQuery(queryForPatientActive);
		while (resultSet.next()) {
			status = true;
		}
		connection.close();
		LOGGER.debug("Patient active status :" + status);
		return status;
	}

//	private boolean isPatientActiveOld(SmsVO smsVO) throws Exception {
//		boolean status = false;
//		Connection connection = ConnectionMannager.GetConnection();
//		Statement statement = null;
//		ResultSet resultSet = null;
//		String fromPhone = smsVO.getFrom().substring(3);
//		String patientId = getPatientIdbyPatientPhone(fromPhone);
//		String campainId = getCampainIdbyPhysicianPhone(smsVO.getTo()) + "";
//		String channelQuery = "SELECT ID FROM PATIENT_CHANNEL_OPTOUT WHERE CHANNEL_NAME = 'SMS' AND PATIENT_ID = '"
//				+ patientId + "' AND CAMPAIGN_ID = " + campainId;
//		String phoneQuery = "SELECT PATIENT_ID FROM CAMPAIGN_PATIENT WHERE CAMPAIGN_ID=" + campainId
//				+ " AND PATIENT_ID IN (SELECT PATIENT_ID FROM PATIENT_PHONE WHERE IS_ACTIVE=1 AND PHONE_NUMBER = '"
//				+ fromPhone + "')";
//		statement = connection.createStatement();
//		resultSet = statement.executeQuery(channelQuery);
//		while (resultSet.next()) {
//			return false;
//		}
//		resultSet = statement.executeQuery(phoneQuery);
//		while (resultSet.next()) {
//			status = true;
//		}
//		connection.close();
//		return status;
//	}

	private String storeReply(SmsVO smsVO, String patientId, String campainIdStr) throws Exception {
		LOGGER.debug("Storing the reply");
		String response = "";
		int campainId = Integer.parseInt(campainIdStr);
		int id = Integer.parseInt(ConnectionMannager.getNextId("SELECT MAX(ID) FROM SMS_RESPONSE"));
		Connection connection = ConnectionMannager.GetConnection();
		PreparedStatement preparedStatement = null;
		String query = "INSERT INTO SMS_RESPONSE (ID, PATIENT_ID, CAMPAIGN_ID, FROM_PHONE, RESPONSE_CODE) VALUES(?,?,?,?,?)";
		preparedStatement = connection.prepareStatement(query);
		preparedStatement.setInt(1, id);
		preparedStatement.setString(2, patientId);
		preparedStatement.setInt(3, campainId);
		preparedStatement.setString(4, smsVO.getFrom());
		preparedStatement.setString(5, smsVO.getBody().trim().toUpperCase());
		int count = preparedStatement.executeUpdate();
		if (count > 0) {
			LOGGER.debug("Inserted Patient SMS Response:" + id);
		}
		connection.close();
		response = getReplyMessage(smsVO.getBody());
		return response;
	}

	private String unsubscribePatientSms(SmsVO smsVO, String patientId, String campainId) throws Exception {
		LOGGER.debug("Adding to unsubscribe list");
		String response = "";
		Connection connection = ConnectionMannager.GetConnection();
		PreparedStatement preparedStatement = null;
		String id = ConnectionMannager.getNextId("SELECT MAX(TO_NUMBER(ID)) FROM PATIENT_CHANNEL_OPTOUT");
		String smsChannelOptoutQuery = "INSERT INTO PATIENT_CHANNEL_OPTOUT (ID, CHANNEL_NAME, STATUS, PATIENT_ID, CAMPAIGN_ID) VALUES(?,?,?,?,?)";
		String patientPhoneQuery = "UPDATE PATIENT_PHONE SET IS_ACTIVE = 0 WHERE PATIENT_ID = ?";
		preparedStatement = connection.prepareStatement(smsChannelOptoutQuery);
		preparedStatement.setString(1, id);
		preparedStatement.setString(2, "SMS");
		preparedStatement.setInt(3, 1);
		preparedStatement.setString(4, patientId);
		preparedStatement.setString(5, campainId);
		preparedStatement.executeUpdate();

		preparedStatement = connection.prepareStatement(patientPhoneQuery);
		preparedStatement.setString(1, patientId);
		preparedStatement.executeUpdate();

		LOGGER.debug("Inserted SMS PATIENT_CHANNEL_OPTOUT ID:" + id);
		connection.close();
		response = getReplyMessage(smsVO.getBody());
		return response;
	}

	private boolean isUnsubscribed(SmsVO smsVO, String patientId, String campainId) throws Exception {
		boolean status = false;
		Connection connection = ConnectionMannager.GetConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		String query = "SELECT ID FROM PATIENT_CHANNEL_OPTOUT WHERE CHANNEL_NAME = 'SMS' AND PATIENT_ID = '" + patientId
				+ "' AND CAMPAIGN_ID = '" + campainId + "'";
		statement = connection.createStatement();
		resultSet = statement.executeQuery(query);
		while (resultSet.next()) {
			status = true;
		}
		connection.close();
		return status;
	}

	private int getCampainIdbyPhysicianPhone(String to) throws Exception {
		int campain_id = 0;
		Connection connection = ConnectionMannager.GetConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		String query = "SELECT CAMPAIGN_ID FROM PHYSICIAN WHERE PHYSICIAN_ID = (SELECT PHYSICIAN_ID FROM PHYSICIAN_PHONE WHERE PHONE_NUMBER = '" + to + "')";
		statement = connection.createStatement();
		resultSet = statement.executeQuery(query);
		while (resultSet.next()) {
			campain_id = resultSet.getInt(1);
		}
		connection.close();
		if (campain_id == 0) {
			throw new Exception("Campain_id not found");
		}
		return campain_id;
	}

	private String getPatientIdbyPatientPhone(String campainId, String from) throws Exception {
		String patient_id = "";
		Connection connection = ConnectionMannager.GetConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		//String query = "SELECT PATIENT_ID FROM PATIENT_PHONE WHERE IS_ACTIVE = 1 AND PHONE_NUMBER = '" + from + "'";
		String query = "SELECT PATIENT_ID FROM CAMPAIGN_PATIENT WHERE CAMPAIGN_ID="+ campainId +" AND PATIENT_ID IN (SELECT PATIENT_ID FROM PATIENT_PHONE WHERE IS_ACTIVE=1 AND PHONE_NUMBER = '" + from +"')";
		statement = connection.createStatement();
		resultSet = statement.executeQuery(query);
		while (resultSet.next()) {
			patient_id = resultSet.getString(1);
		}
		connection.close();
		if (patient_id.isEmpty()) {
			throw new Exception("Patient_id not found");
		}
		return patient_id;
	}

	private String getReplyMessage(String code) throws SQLException {
		String reply = "";
		if (code != null && !code.trim().isEmpty()) {
			code = code.trim();
			Connection connection = ConnectionMannager.GetConnection();
			Statement statement = null;
			ResultSet resultSet = null;
			String query = "select RESPONSE_CONTENT from SMS_ACTION_MESSAGE WHERE UPPER(RESPONSE_CODE) = UPPER('" + code
					+ "')";
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				reply = resultSet.getString(1);
			}
			connection.close();
		}
		return reply;
	}

	private SmsVO parseMessage(String message) {
		SmsVO smsVO = new SmsVO();
		String decStr = URLDecoder.decode(message);
		String[] ele = decStr.split("[&]+");
		String[] keyValue = null;
		for (String string : ele) {
			keyValue = string.split("[=]+");
			if (keyValue != null && keyValue.length > 1) {
				if (keyValue[0].equalsIgnoreCase("MessageSid")) {
					smsVO.setMessageSid(keyValue[1]);
				}
				if (keyValue[0].equalsIgnoreCase("From")) {
					smsVO.setFrom(keyValue[1]);
				}
				if (keyValue[0].equalsIgnoreCase("To")) {
					smsVO.setTo(keyValue[1]);
				}
				if (keyValue[0].equalsIgnoreCase("Body")) {
					smsVO.setBody(keyValue[1]);
				}
				if (keyValue[0].equalsIgnoreCase("MessageStatus")) {
					smsVO.setStatus(keyValue[1]);
				}
			}
		}
		return smsVO;
	}

	public static void main(String[] args) {
		String str = "SmsSid=SM2485de18771945ed917d9baa80a35c05&SmsStatus=sent&MessageStatus=sent&To=%2B919538808247&MessageSid=SM2485de18771945ed917d9baa80a35c05&AccountSid=ACfcb02aa04e9d52f91f28d961c8daa4db&From=%2B447782633007&ApiVersion=2010-04-01";
		new SmsInboundServiceImpl().processStatusRequest(str);
	}
}
