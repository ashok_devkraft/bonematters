package com.indegene.ses.NgagePreAPI.vo;

public class BatchCodeParam {
	private String id;
	private String code;
	private boolean status;
	private String camapignName;

	public BatchCodeParam() {

	}

	public BatchCodeParam(String id, String code, boolean status, String camapignName) {
		this.id = id;
		this.code = code;
		this.status = status;
		this.camapignName = camapignName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getCamapignName() {
		return camapignName;
	}

	public void setCamapignName(String camapignName) {
		this.camapignName = camapignName;
	}

}
